package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/subosito/gotenv"
)

func getFromNasa(d *time.Time) (string, error) {
	url := "https://api.nasa.gov/planetary/apod?api_key=%s&date=%s"
	resp, err := http.Get(fmt.Sprintf(url, os.Getenv("API_KEY"), d.Format("2006-01-02")))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	m := make(map[string]string)
	err = json.Unmarshal(body, &m)
	if err != nil {
		return "", err
	}

	return m["url"], nil
}

func GetPictures(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	from := r.URL.Query().Get("from")

	if from == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"error": "`from` is a required parameter"})
		return
	}

	to := r.URL.Query().Get("to")

	if to == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"error": "`to` is a required parameter"})
		return
	}

	dateFormat := "2006-01-02"
	fromDate, err := time.Parse(dateFormat, from)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"error": "from: " + err.Error()})
		return
	}

	toDate, err := time.Parse(dateFormat, to)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"error": "to: " + err.Error()})
		return
	}

	if fromDate.After(toDate) {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]string{"error": "`from` should not be a later date than `to`"})
		return
	}

	currentDate := fromDate

	concurrency, _ := strconv.Atoi(os.Getenv("CONCURRENT_REQUESTS"))
	sem := make(chan bool, concurrency)

	requestCount := 0
	for !currentDate.After(toDate) {
		requestCount++
		currentDate = currentDate.AddDate(0, 0, 1)
	}
	results := make(chan string, requestCount)
	currentDate = fromDate

	for !currentDate.After(toDate) {
		sem <- true
		go func(d time.Time) {
			defer func() { <-sem }()
			url, err := getFromNasa(&d)
			if err == nil {
				results <- url
			} else {
				fmt.Println(err)
			}
		}(currentDate)
		currentDate = currentDate.AddDate(0, 0, 1)
	}

	for i := 0; i < cap(sem); i++ {
		sem <- true
	}

	urls := []string{}
	for len(results) > 0 {
		url := <-results
		urls = append(urls, url)
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(map[string][]string{"urls": urls})
}

func init() {
	gotenv.Load()
}

func main() {
	http.HandleFunc("/pictures", GetPictures)
	http.ListenAndServe(":"+os.Getenv("PORT"), nil)
}
