FROM golang:1.18

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download
COPY *.go .env ./

RUN go build -o /go-nasa

CMD ["/go-nasa"]
