package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestBadRequests(t *testing.T) {
	urls := []string{"/pictures", "/pictures?to=2021-01-01", "/pictures?from=2022-02-02", "/pictures?from=a&to=b",
		"/pictures?from=2021-02-02&to=2021-02-01", "/pictures?from=2022-02-02&to=2022-02-30",
		"/pictures?from=1950-10-50&to=1952-12-30", "/pictures?from=2022-02-02&to=2022-13-01",
		"/pictures?from=2022-05-13&to=2022-05-32"}
	for _, url := range urls {
		request := httptest.NewRequest(http.MethodGet, url, nil)
		responseRecorder := httptest.NewRecorder()
		GetPictures(responseRecorder, request)
		res := responseRecorder.Result()
		if res.StatusCode != http.StatusBadRequest {
			t.Errorf("expected status code to be %d got %d with url %s", http.StatusBadRequest, res.StatusCode, url)
		}
	}
}

func TestGoodRequest(t *testing.T) {
	url := "/pictures?from=2021-10-11&to=2021-10-13"
	request := httptest.NewRequest(http.MethodGet, url, nil)
	responseRecorder := httptest.NewRecorder()
	GetPictures(responseRecorder, request)
	res := responseRecorder.Result()
	if res.StatusCode != http.StatusOK {
		t.Errorf("expected status code to be %d got %d with url %s", http.StatusOK, res.StatusCode, url)
	}
}
